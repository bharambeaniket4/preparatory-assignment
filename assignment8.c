#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MOVIES_DB  "movies.csv"

typedef struct movie
{
    int id;
    char name[80];
    char genres[220];
}movie_t;

typedef struct node
{
    movie_t data;
    struct node *next;
}node_t;


void display_movie(movie_t *m)
{
    printf("\nID of the movie     : %d",m->id);
    printf("\nName of the movie   : %s",m->name);
    printf("\nGenres of the movie : %s",m->genres);
    printf("\n");

}


node_t *head = NULL;

node_t *create_node(movie_t val)
{
    node_t *newnode = (node_t *)malloc(sizeof(node_t));
    if(newnode == NULL)
    {
        printf("\nMemory allocation failed ");
    }
    else
    {
        printf("\n Memory allocated successfully");
    }
    newnode->data = val;
    newnode->next = NULL;

    return newnode;
}

void add_last(movie_t val)
{
    node_t *newnode = create_node(val);
    if(head == NULL)
    {
        head = newnode;
    }
    else
    {
        node_t *trav = head;
        while(trav->next != NULL)
        {
            trav = trav->next;
        }
        trav->next = newnode;
    }
}

void display_list()
{
    node_t *trav = head;
    while(trav != NULL)
    {
        display_movie(&trav->data);
        trav = trav->next;
    }
}


int parse_movies(char line[],movie_t *m)
{
    int success = 0;
    char *id,*name,*genres;
    id =strtok(line,",\n");
    name =strtok(NULL,",\n");
    genres =strtok(NULL,",\n");
    if(id == NULL || name == NULL || genres == NULL)
    {
        success = 0;
    }
    else
    {
        success = 1;
        m->id = atoi(id);
        strcpy(m->name,name);
        strcpy(m->genres,genres);
        printf("\n");
    }
    return success;
}
 

void load_movies()
{
    movie_t m;
    char line[1024];
    FILE *fp = fopen(MOVIES_DB,"r");
    if(fp == NULL)
    {
        perror("\n Failed to open the CSV file");
        exit(1);
    }
    while(fgets(line,sizeof(line),fp) != NULL)
    {
        //printf("%s",line);
        parse_movies(line,&m);
        //display_movie(&m);
        add_last(m);
    
    }
    fclose(fp);
    
}

void find_movie_by_name()
{
    char name[80];
    int found  =0;
    printf("\n Enter the movie name to be searched : ");
    gets(name);
    node_t *trav = head;

    while(trav != NULL)
    {
        if(strcmp(name,trav->data.name) == 0)
        {
            display_movie(&trav->data);
            found =1 ;
            break;
        }
        trav = trav->next;
    }
    if(!found)
    {
        printf("\nMovie not found");
    }
}

void find_movie_by_genres()
{
    char genres[220];
    int found  =0;
    printf("\n Enter the movie genres to be searched : ");
    gets(genres);
    node_t *trav = head;

    while(trav != NULL)
    {
        if(strstr(trav->data.genres,genres) != 0)
        {
            display_movie(&trav->data);
            found =1 ;
        }
        trav = trav->next;
    }
    if(!found)
    {
        printf("\nMovie not found");
    }
}

int main(void)
{
    load_movies();
    display_list();
    find_movie_by_name();
    find_movie_by_genres();

    return 0; 
}



