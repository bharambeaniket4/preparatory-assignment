#include<stdio.h>
#include<stdlib.h>
#include<conio.h>

struct item_store
{
	int id;
	char name[30];
	float price;
	int qty;

}item;

void add_new_item();
void edit_item();
void show_items();
void search_item();
void delete_item();
int check_in_file(int id);
int is_empty();

void main()
{
 	int choice;
 	while(1)
 	{
		system("cls");
		printf("\nMenu");

	  	printf("\n0. EXIT \n1. Add Item\n2. Edit Item\n3. Display Item\n4. Search Item\n5. Delete Item");
	  	printf("\nEnter your choice: ");
	  	scanf("%d", &choice);

	  	switch (choice)
	  	{
			case 1:
                    add_new_item();
		   		break;

		  	case 2: 
                    edit_item();
                break;

		  	case 3: 
					 show_items();
		   		break;

		  	case 4:
					search_item();
		   		break;

		  	case 5: 
					delete_item();
		   		break;

		  	case 0:
                     exit(0);
		  		break;

		  	default:
					printf("\nInvalid choice\n");
                break;
		}
		getch();
	}
}

int is_empty()
{
	 int count = 0;
	 FILE *fp;
	 fp = fopen("Store.bin", "rb");
	 while (fread(&item, sizeof(item), 1, fp))
	  	count=1;
	 fclose(fp);
	 return count;
}
void add_new_item()
{

	FILE *fp;

	fp = fopen("Store.bin", "ab+");

	if(!is_empty())
		item.id=1;
	else
		item.id++;

	printf("Enter the Item Name : ");
	getchar();
	gets(item.name);
	printf("Enter the Item Price : ");
	scanf("%f", &item.price);
	printf("Enter the Item Quantity : ");
	scanf("%d", &item.qty);
	fwrite(&item, sizeof(item), 1, fp);
	fclose(fp);

	printf("\nNew item added successfully.");
}
int check_in_file(int id)
{
	FILE *fp;

	fp = fopen("Store.bin", "rb");
	while (!feof(fp))
	{
	  	fread(&item, sizeof(item), 1, fp);
	  	if (id == item.id)
	  	{
	   		fclose(fp);
	   		return 1;
	  	}
	}
	fclose(fp);
	return 0;
}
void edit_item()
{
 	FILE *fp_temp, *fp_orig;
 	int id_in_file, edit_id;
 	printf("Enter Item id to edit: ");
 	scanf("%d", &edit_id);
 	if (check_in_file(edit_id) == 0)
 	{
  		printf("Item with Id %d is not Available in the file", edit_id);
 	}
 	else
 	{
  		fp_orig = fopen("Store.bin", "rb");
  		fp_temp = fopen("Temp.bin", "wb");
  		while (fread(&item, sizeof(item), 1, fp_orig))
  		{
   			id_in_file = item.id;
   			if (id_in_file != edit_id)
    			fwrite(&item, sizeof(item), 1, fp_temp);
   			else
   			{
				printf("Enter the Item Name : ");
				getchar();
				gets(item.name);
				printf("Enter the Item Price : ");
				scanf("%f", &item.price);
				printf("Enter the Item Quantity : ");
				scanf("%d", &item.qty);
				fwrite(&item, sizeof(item), 1, fp_temp);
   			}
  		}

        fclose(fp_orig);
        fclose(fp_temp);

        fp_orig = fopen("Store.bin", "wb");
        fp_temp = fopen("Temp.bin", "rb");
        while (fread(&item, sizeof(item), 1, fp_temp))
        {
            fwrite(&item, sizeof(item), 1, fp_orig);
        }
        fclose(fp_orig);
        fclose(fp_temp);
        printf("Item Updated");
        remove("Temp.bin");

        printf("\nItem edited successfully.");
 	}
}
void show_items()
{
	FILE *fp;
    if (!is_empty())
        printf("\nThe file is Empty\n");
    else{

        fp = fopen("Store.bin", "rb");
        printf("\n Item ID\tItem Name\tPrice\tQuantity");

        while (fread(&item, sizeof(item), 1, fp))
        printf("\n %d\t\t%s\t\t%0.2f\t%d\t",item.id,item.name,item.price,item.qty);
        fclose(fp);
    }
}
void search_item()
{
	FILE *fp;
	int search_id, id_in_file;
	printf("\nEnter the Item Id you want to search  : ");
	scanf("%d", &search_id);

	if (check_in_file(search_id) == 0)
		printf("Item with Id %d is not available in the file\n",search_id);
	else
	{
  		fp = fopen("Store.bin", "rb");
  		while (fread(&item, sizeof(item), 1, fp))
  		{
   			id_in_file = item.id;
   			if (id_in_file == search_id)
   			{
    			printf("\n\nID: %d \nName: %s \nPrice: %0.2f \nQuantity: %d ",item.id,item.name,item.price,item.qty);
   			}
  		}
  		fclose(fp);
 	}
}
void delete_item()
{
    show_items();

	FILE *fp_orig;
 	FILE *fp_temp;
 	int del_id, id_in_file;
 	printf("\n\nEnter the Item Id you want to delete : ");
 	scanf("%d", &del_id);
 	fflush(stdin);
 	if (check_in_file(del_id) == 0)
  		printf("Item with Id %d is not available in the file\n", del_id);
 	else
 	{
  		fp_orig = fopen("Store.bin", "rb");
  		fp_temp = fopen("Temp.bin", "wb");
  		while (fread(&item, sizeof(item), 1, fp_orig))
  		{
   			id_in_file = item.id;
   			if (id_in_file != del_id)
    		fwrite(&item, sizeof(item), 1, fp_temp);
  		}
  		fclose(fp_orig);
  		fclose(fp_temp);

  		fp_orig = fopen("Store.bin", "wb");
  		fp_temp = fopen("Temp.bin", "rb");
  		while (fread(&item, sizeof(item), 1, fp_temp))
   			fwrite(&item, sizeof(item), 1, fp_orig);

  		fclose(fp_orig);
  		fclose(fp_temp);
  		remove("Temp.bin");

  		printf("\nItem deleted.");
 	}
}


