#include <stdio.h>
#include <stdlib.h>

typedef struct book {
	int id;
	char name[40];
	int price;
}book_t;

void merge_sort(book_t arr[], int left, int right) {
	int mid, i, j, k, n;
	book_t *temp;
	// base condition: if single element or invalid partition, then return
	if(left == right || left > right)
		return;
	// find the middle of the array
	mid = (left + right) / 2;
	// sort the left partition i.e. left to mid
	merge_sort(arr, left, mid);
	// sort the right partition i.e. mid+1 to right
	merge_sort(arr, mid+1, right);
	// create temp array to accomodate both partition
	n = right - left + 1;
	temp = (book_t*) malloc(n * sizeof(book_t));
	// take index to left part (i), right part (j) & temp array (k)
	i = left;
	j = mid+1;
	k = 0;
	while(i <= mid && j <= right) {
		// compare elements from left and right partition and copy larger element into temp array
		if(arr[i].price > arr[j].price) {
			temp[k] = arr[i];
			i++;
			k++;
		}
		else {
			temp[k] = arr[j];
			j++;
			k++;
		}
	} // repeat until any one partition is completed
	// copy the remaining partition into temp array
	while(i <= mid) {
		temp[k] = arr[i];
		i++;
		k++;
	}
	while(j <= right) {
		temp[k] = arr[j];
		j++;
		k++;
	}
	// overwrite temp array on original array
	for(i=0; i<n; i++)
		arr[left + i] = temp[i];
	// delete the temp array
	free(temp);
}

int main() {
	book_t arr[10] = {
		{7, "Atlas Shrugged", 734}, 
		{1, "The Alchemist", 623}, 
		{5, "The Fountainhead", 532},
		{3, "Wings of Fire", 325},
		{4, "Yugandhar", 587},
		{8, "Mrityunjay", 973},
		{9, "Rich dad & Poor dad", 534},
		{10, "Monk who sold his ferrari", 238},
		{6, "Chhava", 592},
		{2, "The secret", 351}
	};
	int i, len = 10;
	merge_sort(arr, 0, len-1);
	for(i=0; i<len; i++)
		printf("%d, %s, %d\n", arr[i].id, arr[i].name, arr[i].price);
	printf("\n");
	return 0;
}
