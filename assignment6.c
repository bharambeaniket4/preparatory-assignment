#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct student {
	int roll;
	char name[30];
	int std;
	char subjects[6][20];
	int marks[6];

}student_t;

void accept_student(student_t *s) 
{
	printf("roll: ");
	scanf("%d", &s->roll);
	printf("name: ");
	scanf("%s", s->name);
	printf("std: ");
	scanf("%d", &s->std);

    
    for(int i=0; i<6; i++)
    {
        printf("subject: ");
        scanf("%s", s->subjects[i]);
        printf("marks: ");
        scanf("%d", &s->marks[i]);
    }

}
void display_student(student_t *s) 
{
    printf("%d, %s, %d", s->roll, s->name, s->std);
    for(int i=0; i<6; i++)
    {
        printf("\nsubject: %s", s->subjects[i]);
        printf("\nmarks: %d", s->marks[i]);
    }
  
}

#define SIZE	5

typedef struct cirque {
	student_t arr[SIZE];
	int rear;
	int front;
	int count;
}cirque_t;


void cq_init(cirque_t *q) {
	memset(q->arr, 0, sizeof(q->arr));
	q->front = -1;
	q->rear = -1;
	q->count = 0;
}

void cq_push(cirque_t *q, student_t s) {
	q->rear = (q->rear+1) % SIZE;
	q->arr[q->rear] = s;
	q->count++;


}
int cq_full(cirque_t *q) {
	return q->count == SIZE;
}

void cq_pop(cirque_t *q) {
	q->front = (q->front+1) % SIZE;	
	q->count--;
}

student_t cq_peek(cirque_t *q) {
	int index = (q->front+1) % SIZE;
	return q->arr[index];
}

int cq_empty(cirque_t *q) {
	return q->count == 0;
}


int main() {

	int choice;
    student_t s;
    //accept_student(&s);
    //display_student(&s);
    cirque_t q;
	student_t temp;
    cq_init(&q);

 	do {
		printf("\n0. exit\n1. push\n2. pop\n3. peek\nenter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: // push
			if(cq_full(&q))
				printf("queue is full.\n");
			else {
				accept_student(&temp);
				cq_push(&q, temp);
			}
			break;
		case 2: // pop
			if(cq_empty(&q))
				printf("queue is empty.\n");
			else {
				temp = cq_peek(&q);
				cq_pop(&q);
				printf("popped: ");
				display_student(&temp);
			}
			break;
		case 3: // peek
			if(cq_empty(&q))
				printf("queue is empty.\n");
			else {
				temp = cq_peek(&q);
				printf("topmost: ");
				display_student(&temp);
			}
			break;
		}

	} while(choice != 0); 
	return 0;
}

