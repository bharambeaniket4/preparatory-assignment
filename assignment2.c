#include<stdio.h>
#include<stdlib.h>

typedef enum{EXIT,STRCPY,STRCAT,STRCMP,STRREV}SIMUOP;

SIMUOP menu_choice()
{
    SIMUOP choice;
    printf("\n 0. Exit");
    printf("\n 1. strcpy simulation");
    printf("\n 2. strcat simulation");
    printf("\n 3. strcmp simulation");
    printf("\n 4. strrev simulation");
    printf("\nEnter Choice  : ");
    scanf("%d",&choice);
    return choice;
}

char *my_strcpy(char *dest,char *src)
{   
    for(int i=0;dest[i]!='\0';i++)
    {
        for(int j=0;src[j]!='\0';j++)
        {
            dest[i]=src[j];
            i++;
        }
        dest[i]='\0';
    }

   return dest;
}

char *my_strcat(char *dest, const char *src)  //char *strcat(char *dest, const char *src)
{
    char *d = dest;        
    while(*d != '\0')
        d++;   
    while(*src!='\0')
    {
            *d = *src ; 
            src++;         
            d++;       
    }
    *d = '\0';
    return dest;
} 

int my_strcmp(const char *dest,const char *src)
{
    int i;
    while(*dest != '\0' && *src != '\0')
    {
        if(*dest != *src)
            break;
        *dest++;
        *src++;
    }
    return *dest - *src;
}

char *my_strrev(char *str,char *dest)
{
    //char dest[50];
    size_t length = 0 ;
    for(int i=0;str[i] != '\0';i++)
    {
        length++;
    }
    int j=0;
    for(int i=length-1;i>=0;i--)
    {
        //dest[j] = str [i];
        *(dest + j) = *(str + i);
        j++;
    }
    dest[j] = '\0';
    return dest;
}



int main(void)
{
    char s1[30],s2[30],*cptr;
    int len =0;
    int diff;
    SIMUOP choice;
    while((choice = menu_choice()) != EXIT) 
    {
        switch(choice)
        {
            case STRCPY:
                        printf("Specify String s1   : ");
                        scanf("%*c");
                        gets(s1);
                        printf("Specify String s2   : ");
                        //scanf("%*c");
                        gets(s2);
                        cptr = my_strcpy(s1,s2);  
                        printf("\nNew Str1 afercopy :");
                        puts(cptr);
                        //printf("string s1   : %s",cptr);
                break;

            case STRCAT:
                        printf("Specify String s1   : ");
                        scanf("%*c");
                        gets(s1);
                        printf("Specify String s2   : ");
                        //scanf("%*c");
                        gets(s2);
                        cptr = my_strcat(s1,s2);   
                        printf("Concatenated string : ");
                        puts(cptr);    
                break;

            case STRCMP:
                        printf("Specify String s1   : ");
                        scanf("%*c");
                        gets(s1);
                        printf("Specify String s2   : ");
                        //scanf("%*c");
                        gets(s2);
                        diff = my_strcmp(s1,s2);
                        if(diff > 0 )
                            printf("%s is grater than %s",s1,s2);
                        else if(diff < 0)
                            printf("%s is less than %s",s1,s2);
                        else
                            printf("%s is equals %s",s1,s2);
                        break;  
                break;

            case STRREV:
                        printf("Specify String s1   : ");
                        scanf("%*c");
                        gets(s1);
                        cptr = my_strrev(s1,s2); 
                        printf("After appling strrev : %s",cptr);
                        //puts(cptr);
                break;

        }
    }

    return 0;
}


    

