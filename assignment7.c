
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct BankAcc
{
    int id;
    char TypeAcc[10];
    char AccHolderName[100];
    char Address[100];
    char ContactNum[12];

    struct BankAcc *prev;
    struct BankAcc *next;
} Node;

Node *head=NULL;

void accept_data(int *id,char *type,char *name,char *address,char *contNum);
void add_node_at_first(int id, char *Type,char *accN,char *add, char *ContNum);
void add_node_at_last(int id, char *type,char *accN,char *add, char *ContNum);
void display_forward(void);
void display_backward(void);
void find_by_accid();
void find_by_accName();
void delete_all(void);

int main()
{
int choice;

    do
    {
        int id;
    char type[20], name[100], address[100],contNum[12];
        printf("Select any option from the following list\n");
        printf("1-for adding data at first position\n");
        printf("2-for adding data at last position\n");
        printf("3-Display the data in forward direction\n");
        printf("4-Display the data from backwards\n");
        printf("5-Find by account id\n");
        printf("6-Find by account name\n");
        printf("7-Delete everything\n");
        printf("0-exit\n");
        scanf("%d",&choice);


        switch(choice)
        {
        case 1:
            accept_data(&id,type,name,address,contNum);
            add_node_at_first(id,type,name,address,contNum);
            break;
        case 2:
            accept_data(&id,type,name,address,contNum);
            add_node_at_last(id,type,name,address,contNum); 
            break;
        case 3:
            display_forward();
            break;
        case 4:
            display_backward();
            break;
        case 5:
            find_by_accid();           
            break;
        case 6:
            find_by_accName();           
            break;
        case 7:
            delete_all();            
            break;

        }
    }while(choice !=0);

    return 0;
}

void accept_data(int *id,char *type,char *name,char *address,char *contNum)
{

    printf("Enter the Account Id\n");
    scanf("%d", id);

    printf("Enter the type of Account\n");
    getchar();
    gets(type);
    printf("Enter the account holder's name\n");
    gets(name);

    printf("Enter your address\n");
    gets(address);
    printf("Enter your Mobile number\n");
    gets(contNum);

}
Node* create_node(int id, char *type,char *accN,char *add, char *contNum)
{
    Node *temp=(Node*)malloc(sizeof(Node));
    if(temp==NULL)
    {
        printf("Malloc failed\n");
        exit(1);
    }
    temp->id= id;
    strcpy(temp->TypeAcc,type);
    strcpy(temp->AccHolderName,accN);
    strcpy(temp->Address,add);
    strcpy(temp->ContactNum,contNum);
    temp->prev=NULL;
    temp->next=NULL;
    return temp;
}
void add_node_at_first(int id, char *type,char *accN,char *add, char *ContNum)
{
    //printf("IN ADD: %d %s %s %s %s",id,type,accN,add,ContNum);
    Node *newNode=create_node(id,type,accN,add,ContNum);
    if(head==NULL)
    {
        head=newNode;
        newNode->next=NULL;
    }

    else
    {
        head->prev=newNode;
        newNode->next=head;
        newNode->prev=NULL;
        head=newNode;
    }

}
void add_node_at_last(int id, char *type,char *accN,char *add, char *ContNum)
{
    Node *newNode=create_node(id,type,accN,add,ContNum);
    if(head==NULL)
    {
        head=newNode;
        newNode->next=NULL;
    }

    else
    {
        Node *trav=head;
        while(trav->next!=NULL)
            trav=trav->next;
        trav->next=newNode;
        newNode->prev=trav;
    }
}
void display_forward(void)
{
    if(head!=NULL)
    {
        Node *trav=head;

        while(trav!=NULL)
        {
            printf("%d       %s       %s       %s       %s       \n",trav->id,trav->TypeAcc,trav->AccHolderName,trav->Address,trav->ContactNum);
            trav=trav->next;
        }
    }
    else
        printf("The DLL is empty!!!!\n");
}
void display_backward(void)
{
    if(head!=NULL)
    {
        Node *trav=head;
        while(trav->next!=NULL)
            trav=trav->next;
        while(trav!=NULL)
        {
            printf("%d       %s       %s       %s       %s       \n",trav->id,trav->TypeAcc,trav->AccHolderName,trav->Address,trav->ContactNum);
            trav=trav->prev;
        }
    }
    else
        printf("The DLL is empty!!!!\n");
}
void find_by_accid()
{
    int id;
    printf("enter the id by which you want to search\n");
    scanf("%d",&id);
    Node *trav=head;
    int flag=0;
    while(trav!=NULL)
    {
        if(trav->id==id)
        {
            flag=1;
            break;
        }
        trav=trav->next;
    }
    if(flag==1)
        printf("Record Found!!!!!\n");
    else
        printf("No matching record found!!!!\n");
}
void find_by_accName()
{
    char name[100];
    printf("enter the full name by which you want to search\n");
    getchar();
    gets(name);
    Node *trav=head;
    int flag=0;
    while(trav!=NULL)
    {
        if(!strcmp(trav->AccHolderName,name))
        {
            flag=1;
            break;
        }
        trav=trav->next;
    }
    if(flag==1)
        printf("Record Found!!!!!\n");
    else
        printf("No matching record found!!!!\n");
}
void delete_all(void)
{
    head=NULL;
}
