#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef enum{EXIT,ADD,DELETE,FIND_MAX,FIND_MIN,SUM}ARROP;
ARROP menu_operation()
{
    ARROP choice;
    printf("\n 0. EXIT ");
    printf("\n 1. ADD ");
    printf("\n 2. DELETE ");
    printf("\n 3. FIND_MAX ");
    printf("\n 4. FIND_MIN ");
    printf("\n 5. SUM ");
    printf("\nENTER THE CHOICE  : ");
    scanf("%d",&choice);
    return choice;
}

int check_full(int *arr)
{
    int s=0;
    for(int i =0; i<10;i++)
    {
        if(arr[i]!=0)
            s++;
        else
            continue;
    }
    if(s==10)
        return 1;
    else
        return 0;
}
int check_empty(int *arr)
{
    int s=0;
    for(int i =0; i<10;i++)
    {
        if(arr[i]==0)
            s++;
        else
            continue;
    }
    if(s==10)
        return 1;
    else
        return 0;
    
}

void add_fun(int *arr,int a)
{
    int b;
    printf("\n Available locations to add element :- ");
    
    for(int i=0;i<=9;i++)
    {
        if(arr[i]==0)
        {
            printf("\narr[%d]  :   %d",i,arr[i]);
        }
        else
        {
            continue;
        }
    }
    printf("\nSelect location to add element : ");
    scanf("%d",&b);
    if(b>=10)
        printf("\nInvalid Array Location ");
    else
    {
        arr[b] = a;
        printf("\nUpdated array :");
        for(int i=0;i<=9;i++)
        {
            printf("\narr[%d]  :   %d",i,arr[i]);
        }
    }

}

void delete_fun(int *arr)
{
    int c;
    printf("\nArray displayed :-");
    for(int i=0;i<=9;i++)
    {
        printf("\narr[%d]  :   %d",i,arr[i]);
    }
    
    printf("\nSelect the location to delete :-");
    scanf("%d",&c);
    if(c>=10)
        printf("\n Invalid Location ");
    else
    {
        arr[c] = 0;
        printf("\nUpdated Array : ");
        for(int i=0;i<=9;i++)
        {
            printf("\narr[%d]  :   %d",i,arr[i]);
        }
    }
}

void find_max(int *arr)
{
    int index = 0;
    int max = arr[0];
    for(int i=0;i<=9;i++)
    {
        if(arr[i]>max)
        {
            max=arr[i];
        }
    }
    for(int i=0;i<=9;i++)
        if(max == arr[i])
        {
            printf("\nThe max elememt in array   :");
            printf("\narr[%d] :   %d",i,max);

        }   
}

void find_min(int *arr)
{
    int index = 0;
    int min = arr[0];
    for(int i=0;i<=9;i++)
    {
        if(arr[i]<min)
        {
            min=arr[i];
        }
    }
    for(int i=0;i<=9;i++)
        if(min == arr[i])
        {
            printf("\nThe min element in array   :");
            printf("\narr[%d] :   %d",i,min);

        }   
}


void sum_ele(int *arr)
{
    int sum = 0;
    for(int i=0;i<=9;i++)
    {
        sum = sum + arr[i];
    }
    printf("\n Sum of array elements is :  %d",sum);
}

int main(void)
{
    int a,check,max;
    int arr[10];
    int *rarr;
    memset(arr,0,sizeof(arr));
    ARROP choice;
    while((choice = menu_operation()) != EXIT)
    {
        switch(choice)
        {
            case ADD:
                    check = check_full(arr);
                    if(check == 1)
                        printf("\nArray is fully occupied");
                    else
                    {
                        printf("\nEnter the value to be added in array  : ");
                        scanf("%d",&a);
                        add_fun(arr,a);
                    }
            break;

            case DELETE:  
                    check = check_empty(arr);
                    if(check == 1)
                        printf("\nArray is Empty ");
                    else
                        delete_fun(arr);           
            break;

            case FIND_MAX:
                    check = check_empty(arr);
                    if(check == 1)
                        printf("\nArray is Empty ");
                    else
                    find_max(arr); 
            break;

            case FIND_MIN:
                    check = check_empty(arr);
                    if(check == 1)
                        printf("\nArray is Empty ");
                    else
                    find_min(arr); 
            break;

            case SUM:
                    check = check_empty(arr);
                    if(check == 1)
                        printf("\nArray is Empty ");
                    else
                    sum_ele(arr); 
            break;  
        }
    }
    return 0;       
}