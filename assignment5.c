#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct emp acceptRecord (struct emp *e1);
struct emp showRecord(struct emp *e1);
int isLeap(int y);
int offsetDays(int d, int m, int y);
void revoffsetDays(int offset, int y, int *d, int *m);
void findAge(int *birthDate,int *joinDate);
void experience(int *todayDate,int *joinDate);
void probationPeriod(int d1, int m1, int y1, int x);
struct date
{
    int dd;
    int mm;
    int yyyy;
};
struct emp 
{
    int id;
    char name[20];
    char address[30];
    double salary;
    struct date birthDate;
    struct date joinDate;
    struct date todayDate;
};
struct emp acceptRecord (struct emp *e1)
{
    printf("Enter your id : ");
    scanf("%d%*c",&e1->id);

    printf("Enter your name : ");
    gets(e1->name);

    printf("Enter your address : ");
    gets(e1->address);

    printf("Enter your salary : ");
    scanf("%ld",&e1->salary);

    printf("Enter your birth date : \n\tdate : ");
    scanf("%d",&e1->birthDate.dd);
  
    scanf("%d",&e1->birthDate.mm);

    scanf("%d",&e1->birthDate.yyyy);

    printf("Enter your joining date : \n\tdate : ");
    scanf("%d",&e1->joinDate.dd);
   
    scanf("%d",&e1->joinDate.mm);
   
    scanf("%d",&e1->joinDate.yyyy);

    printf("Enter today date : \n\tdate : ");
    scanf("%d",&e1->todayDate.dd);
    
    scanf("%d",&e1->todayDate.mm);
    
    scanf("%d",&e1->todayDate.yyyy);
}
struct emp showRecord(struct emp *e1)
{
    printf("Id : %d \n",e1->id);
    printf("Name : %s\n",e1->name);
    printf("Adrress : %s\n",e1->address);
    printf("Salary : %d\n",e1->salary);
    printf("Birth Date : %d-%d-%d\n",e1->birthDate.dd,e1->birthDate.mm,e1->birthDate.yyyy);
    printf("Join Date : %d-%d-%d\n",e1->joinDate.dd,e1->joinDate.mm,e1->joinDate.yyyy);
}

int isLeap(int y)
{
    if (y % 100 != 0 && y % 4 == 0 || y % 400 == 0)
        return 1;

    return 0;
}

int offsetDays(int d, int m, int y)
{
    int offset = d;

    switch (m - 1)
    {
    case 11:
        offset += 30;
    case 10:
        offset += 31;
    case 9:
        offset += 30;
    case 8:
        offset += 31;
    case 7:
        offset += 31;
    case 6:
        offset += 30;
    case 5:
        offset += 31;
    case 4:
        offset += 30;
    case 3:
        offset += 31;
    case 2:
        offset += 28;
    case 1:
        offset += 31;
    }

    if (isLeap(y) && m > 2)
        offset += 1;

    return offset;
}

void revoffsetDays(int offset, int y, int *d, int *m)
{
    int month[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    if (isLeap(y))
        month[2] = 29;

    int i;
    for (i = 1; i <= 12; i++)
    {
        if (offset <= month[i])
            break;
        offset = offset - month[i];
    }

    *d = offset;
    *m = i;
}

void probationPeriod(int d1, int m1, int y1, int x)
{
    int offset1 = offsetDays(d1, m1, y1);
    int remDays = isLeap(y1) ? (366 - offset1) : (365 - offset1);

    int y2, offset2;
    if (x <= remDays)
    {
        y2 = y1;
        offset2 = offset1 + x;
    }

    else
    {
        x -= remDays;
        y2 = y1 + 1;
        int y2days = isLeap(y2) ? 366 : 365;
        while (x >= y2days)
        {
            x -= y2days;
            y2++;
            y2days = isLeap(y2) ? 366 : 365;
        }
        offset2 = x;
    }

    int m2, d2;
    revoffsetDays(offset2, y2, &d2, &m2);

    printf("Date when his probation period is over:", d2);
    printf("%d/", d2);
    printf("%d/", m2);
    printf("%d\n", y2);
}

void findAge(int *birthDate,int *joinDate)
{
    int **b=&birthDate;
    int **j=&joinDate;
    int age = **j - **b;
    printf("Age :%d \n",age);
}

void experience(int *todayDate,int *joinDate)
{
    int **t=&todayDate;
    int **j=&joinDate;
    int experience = **t - **j;
    printf("Experience : %d month \n",experience);
}
void probationPeriod(int d1, int m1, int y1, int x)
{
    int offset1 = offsetDays(d1, m1, y1);
    int remDays = isLeap(y1) ? (366 - offset1) : (365 - offset1);

    int y2, offset2;
    if (x <= remDays)
    {
        y2 = y1;
        offset2 = offset1 + x;
    }

    else
    {
        x -= remDays;
        y2 = y1 + 1;
        int y2days = isLeap(y2) ? 366 : 365;
        while (x >= y2days)
        {
            x -= y2days;
            y2++;
            y2days = isLeap(y2) ? 366 : 365;
        }
        offset2 = x;
    }

    int m2, d2;
    revoffsetDays(offset2, y2, &d2, &m2);
}


int main ( void )
{
    struct emp emp1;
    acceptRecord(&emp1);
    showRecord(&emp1);
    findAge(&emp1.birthDate.yyyy,&emp1.joinDate.yyyy);
    experience(&emp1.todayDate.mm ,&emp1.joinDate.mm );
    int x= 90;
    probationPeriod(emp1.joinDate.dd ,emp1.joinDate.mm ,emp1.joinDate.yyyy , x );
    return 0;
}
